class SecretData {

    constructor(id, encryptedMessage, status){

        this.id = id;
        this.encryptedMessage = encryptedMessage;
        this.status = status;

    }

}

module.exports = SecretData;