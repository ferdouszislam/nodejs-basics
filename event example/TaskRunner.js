const EventEmitter = require('events');

class TaskRunner extends EventEmitter{


    constructor(){
        super();

        this.task_id = 0;

        this.smallTaskTag = "small-task";
        this.mediumTaskTag = "medium-task";
        this.bigTaskTag = "big-task";
    }

    smallTask(taskDescription){
        
        this.__task(this.smallTaskTag, taskDescription, 1000);

    }

    mediumTask(taskDescription){

        this.__task(this.mediumTaskTag, taskDescription, 2000);
        
    }

    bigTask(taskDescription){
        
        this.__task(this.bigTaskTag, taskDescription, 3000);

    }

    // '__method()' == indicates private method
    __task(taskTag, taskDescription, interval){

        const startTimeStamp = Date.now();

        setTimeout(() => {
            
            const timePassed = (Date.now()-startTimeStamp)/1e3;
            
            // fire an event
            this.emit(
                taskTag, 
                "Task-"+this.task_id+": "+taskDescription+" [runtime="+timePassed+"s]"
            );

            this.task_id++;

        }, interval);

    }

}

// making class import-able
module.exports = TaskRunner;