const TaskRunner = require('./TaskRunner.js');

const taskRunner = new TaskRunner();

// hook up the callback listeners
taskRunner.on(taskRunner.smallTaskTag, (responseData) => console.log(responseData) );
taskRunner.on(taskRunner.mediumTaskTag, (responseData) => console.log(responseData) );
taskRunner.on(taskRunner.bigTaskTag, (responseData) => console.log(responseData) );


taskRunner.bigTask("convert speech to text and run NER");

taskRunner.smallTask('send text message');

taskRunner.mediumTask('resize image under 5MB');
