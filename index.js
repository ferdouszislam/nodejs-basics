const http = require('http');
const path = require('path');
const uuid = require('uuid');


// genearate api json data
const SecretData = require('./SecretData');
const secretDatas = new Array();
const dataSize =  Math.floor((Math.random()*10));
for(let i=1; i<=dataSize; i++)
    secretDatas.push(new SecretData(i, uuid.v4(), i%2===0 ? true : false ));


// setup & start the server
const PORT = process.env.PORT || 5000;
const server = http.createServer( (request, response) => {

    const error = () => {
        response.write('404 not found!');
        response.end();
    };

    if(request.url === '/api/top-secret-data'){

        response.writeHead(200, 'application/json');
        response.write(JSON.stringify(secretDatas));

    }

    else if( path.dirname(request.url) === `/api/top-secret-data`){
    
        let id = path.basename(request.url);
        id = parseInt(id);
        
        if(id>dataSize || id<0 || Number.isNaN(id)){
            error();
            return ;
        }

        response.writeHead(200, 'application/json');
        response.write(JSON.stringify(secretDatas[id]));
        
    }

    else {
        error();
        return ;
    }

    response.end();

});

server.listen(PORT, () => console.log('server running on port '+PORT) );